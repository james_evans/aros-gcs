from twython import Twython #twitter integration
import random
import json

#https://projects.raspberrypi.org/en/projects/getting-started-with-the-twitter-api/8

def post_image(image_file_path,message):
    captions = [
        "Great pic!",
        "Lookin' good!",
        "Stylish!",
        "Nice pic!",
        "Cool photo!",
        "Awesome!"
    ]

    #Add random captions if blank
    print(len(message))
    if len(message) == 0:
        message = random.choice(captions)

    #Add hashtags
    message = message + " #AROSImaging #SFU"

    # Don't share these keys pls:
    # NOTE [OCT 28, 2020]: All keys are now invalid (have been regenerated), since this repo is going public. New keys are available at apps.twitter.com
    APP_KEY = '1122213610405515264-FG5KW272o4yfKQRM4uu34BXJgGGJy2'
    APP_SECRET = 'VfPDS28RIWlTY9EAkn01mAuCbGNqZZRi7GyLCDnTv5X7E'
    CONSUMER_KEY = 'iB5qqWnMwWynjEEeX6MsuINzi'
    CONSUMER_SECRET = 'NAVsNAV4LY2nZs6W9mmfj4kGqNmTw8EXCgNAK50cQTAdMd5ggc'

    twitter = Twython(CONSUMER_KEY,CONSUMER_SECRET,APP_KEY, APP_SECRET)

    image = open(image_file_path,'rb')

    response = twitter.upload_media(media=image)
    media_id = [response['media_id']]

    twitter.update_status(status=message, media_ids=media_id)

