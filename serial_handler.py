"""
 Adapted from aros-capture.py file and stackoverflow code
 process_csv:
 Uses data from a csv file to build a jpeg image and saves to disk

 snap:
 Opens serial port to camera, transmits the "snap" command, and captures all incoming data until the data stops.
 Writes data to csv file on disk. Calls process_csv at end and returns image location.

 serial_ports:
 Returns list of active serial comm ports
 Credit: https://stackoverflow.com/questions/12090503/listing-available-com-ports-with-python
"""

import sys
import glob
import serial
import os
import numpy as np
import datetime
import csv
from PIL import Image

def generate_unique_file_name(outputDirectory="."):
    index = 0
    while(1):
        fileName = 'capture-{0}-{1}'.format(datetime.datetime.now().strftime("%d-%H-%M"), index)
        if not os.path.isfile(outputDirectory + "/csv/" + fileName + ".csv") and not os.path.isfile(outputDirectory + fileName + ".jpeg"):
            return fileName
        index = index + 1

def convert_rgb565(rgb565):
    return [255*((rgb565 >> 11) & 0x1F)/31, 255*((rgb565 >> 5) & 0x3F)/63, 255*(rgb565 & 0x1F)/31]

def process_csv(fileName,width,height,outputDirectory="."):

    with open(outputDirectory + "/csv/" + fileName + ".csv") as csvFile:

        rawBytes = [int(x) for x in csvFile.read().split(",")]

        print("{0}.csv contains {1}/{2} bytes expected for {3}x{4} image.".format(fileName, len(rawBytes)-1, width*height*2, width, height))

        rgb565Data = []
        for i in range(len(rawBytes)):
            if i % 2:
                rgb565Data[-1] = (rawBytes[i] << 8) | rgb565Data[-1]
            else:
                rgb565Data.append(rawBytes[i])

        n = 0
        m = 0

        pixelArray = np.zeros((height,width,3), 'uint8')

        for rgb565 in rgb565Data:

            pixelArray[n, m] = convert_rgb565(rgb565)

            m += 1
            if m == width:
                m = 0
                n += 1
                if n == height:
                    break

        print("Writing image as {0}.jpeg".format(fileName))

        img = Image.fromarray(pixelArray)

        img.save(outputDirectory + "/" + fileName + '.jpeg')
        return outputDirectory + "/" + fileName + '.jpeg'

def snap(serialPort, width, height, outputDirectory="."):

    baudRate = 115200

    print("Attempting to open {0} at {1} baud for TX/RX...".format(serialPort, baudRate))

    with serial.Serial(serialPort, baudRate, timeout = 5) as ser:
        print("Serial connection opened successfully.")

        count = 0
        rawInts = []
        fileName = generate_unique_file_name(outputDirectory)

        ser.write("snap\r".encode())

        print("Waiting for new data. (Ctrl-C to force break)")
        print("Bytes received: 0 ({0} expected)".format(width*height*2), end='\r')

        try:
            while(1):
                pixRaw = ser.read(1)

                if(len(pixRaw) > 0):
                    pix = int.from_bytes(pixRaw, byteorder='little')
                    print("Bytes received: {0} ({1} expected)".format(count, width*height*2), end='\r')
                    rawInts.append(pix)
                    count = count + 1

                elif count > 0:
                    print("")
                    break

        except KeyboardInterrupt:
            pass

         #make [directory]/csv/ directory if doesn't already exist
        if not os.path.exists(outputDirectory + "/csv/"):
            os.makedirs(outputDirectory + "/csv/")

        print("Writing data to {0}.csv...".format(outputDirectory + "/csv/" + fileName))

        with open(outputDirectory + "/csv/" + fileName + '.csv', 'w', newline='') as csvFile:
            writer = csv.writer(csvFile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(rawInts[:])

        print("Done writing csv.")

    return process_csv(fileName, width, height, outputDirectory) #returns filepath of image

def serial_ports(): #See credit above. Returns available serial comm ports for selection.
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

