# Adapted from Rich A's read_raw_dump.py file
# Input arguments: .csv file location, output directory, expected width and height
# Outputs: .jpg saved in specified directory, returns .jpg filepath

import sys
import numpy as np
from PIL import Image



def convert_rgb565(rgb565):

    return [255*((rgb565 >> 11) & 0x1F)/31,   255*((rgb565 >> 5) & 0x3F)/63,   255*(rgb565 & 0x1F)/31]



def read_csv(csv_filepath,output_filepath,width,height,filetype):
    with open(csv_filepath) as csv_file:

        raw_bytes = [int(x) for x in csv_file.read().split(",")]

        print("{0}/{1} expected bytes".format(len(raw_bytes), width*height*2))

        rgb565_data = []
        for i in range(len(raw_bytes)):
            if i % 2:
                rgb565_data[-1] = (rgb565_data[-1] << 8) | raw_bytes[i]
            else:
                rgb565_data.append(raw_bytes[i])

        n = 0
        m = 0

        pixel_array = np.zeros((height,width,3), 'uint8')

        for rgb565 in rgb565_data:

            pixel_array[m, n] = convert_rgb565(rgb565)

            n += 1
            if n == width:
                n = 0
                m += 1
                if m == height:
                    break

        img = Image.fromarray(pixel_array)

        if filetype == 1: #JPEG
            img.save(output_filepath + "/testimage.jpg")
            return (output_filepath + "/testimage.jpg")
        if filetype == 2:  # PNG
                img.save(output_filepath + "/testimage.png", 'PNG')
                return (output_filepath + "/testimage.png")
        #img.save(sys.argv[1] + '.jpeg')