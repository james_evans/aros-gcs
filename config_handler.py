import configparser  # For saving configuration files


def load_user_values():
    cfg = configparser.ConfigParser()

    cfg.read('config.ini')
    try:
        values = cfg['user']
    except:
        reset_default_values()
        cfg.read('config.ini')
        values = cfg['user']
        print("Config file generated")

    return values


def save_current_values(values):
    cfg = configparser.ConfigParser()

    cfg['user'] = values

    with open('config.ini', 'w') as configfile:
        cfg.write(configfile)
    configfile.close()


def reset_default_values():
    cfg = configparser.ConfigParser()

####### DEFINE PARAMETER DEFAULTS HERE #######

    cfg['user'] = {'v_e1': 100,
                   'v_e2': 100,
                   'v_e3': 100,
                   'v_e4': 100,
                   'n_e1': 200,
                   'n_e2': 200,
                   'n_e3': 200,
                   'n_e4': 200, }

    with open('config.ini', 'w') as configfile:
        cfg.write(configfile)
    configfile.close()

    values = cfg['user']
    return values