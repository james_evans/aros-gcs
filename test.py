from tkinter import *
import tkinter.messagebox

versionInfo = "v0.00"

splash = Tk()  #Splash screen
splash.title("AROS GSS - Splash")
splash.resizable(FALSE,FALSE)
root = Tk()    #Main screen
root.title("AROS GSS")

# Functions
def doNothing():
    print("Test")


#Splash ----------------------

#splash_frame = Frame(splash,width = 500, height = 400)
splash_image = PhotoImage(file = "splash.gif")
splash_label = Label(splash,image = splash_image)
#splash_frame.pack()
splash_label.pack()


splash_text1 = Label(splash,text = "AROS Ground Station Software")
splash_text2 = Label(splash,text = versionInfo)
splash_text1.pack(side = LEFT)
splash_text2.pack(side = RIGHT)

splash.after(5000, splash.destroy)


#Root ----------------------

#Menu definition:
menu = Menu(root)
root.config(menu=menu)

menu_1 = Menu(menu)
menu.add_cascade(label = "File",menu=menu_1)
#Menu contents ------
menu_1.add_command(label = "New", command = doNothing)
menu_1.add_command(label = "Old", command = doNothing)
menu_1.add_separator()
menu_1.add_command(label = "Quit", command = root.quit)

#Root Contents

imageFrame = Frame(root,relief = SUNKEN, bd = 1)
imageFrame.pack(side = LEFT)
controlFrame = Frame(root, relief = SUNKEN, bg = "grey")
controlFrame.pack(side = TOP, fill = Y)

canvas = Canvas(imageFrame, width = 600, height = 600)
circle = canvas.create_oval(0,0,600,600,fill = "grey")
canvas_photo = PhotoImage(file = "splash.gif")
#canvas_image = canvas.create_image(canvas, image = canvas_photo)
canvas.pack()


captureB = Button(controlFrame, text = "Capture", command = doNothing)
twitterB = Button(controlFrame, text = "Upload to Twitter", command = doNothing)
twitterEntry = Entry(controlFrame)

captureB.pack(side = TOP)
twitterB.pack(side = TOP)
twitterEntry.pack(side = TOP)

#Status Bar Contents

status = Label(imageFrame,text = "Status Bar", bd = 1, relief = SUNKEN, anchor = W)
status.pack(side = BOTTOM, fill = X)







#Place splash screen in center of monitor
windowWidth = splash.winfo_reqwidth()
windowHeight = splash.winfo_reqheight()
positionRight = int(splash.winfo_screenwidth()/2 - 250)
positionDown = int(splash.winfo_screenheight()/2 - 140)
splash.geometry("+{}+{}".format(positionRight, positionDown))



root.mainloop()