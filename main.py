from tkinter import * #tkinter is the main GUI engine
from tkinter import filedialog
from PIL import Image, ImageTk #used to display .png/.jpg images which tk doesn't support

from twython_handler import post_image #function to send image to twitter
import csv_handler #handler for processing .csv data from module into jpgs
import serial_handler #deals with serial communication, including capture requests and data download
import config_handler #for dealing with configuration files

# TODO:
# Add Canvas scalability
# Write .bash script to install required packages on fresh computer
# Add "data directory" to config loading on startup
# Add nice icons for capture/download section on sidebar
# Cleanup .csv file conversion code
# Debug weird banding issue with green channel
# Add AROS logo to top left corner (transparency?)
# There was something else but I forgot what it was


#--- Parameters ---
versionInfo = "AGCS - v0.5"

entry_width = 6
padding = 3


# Colour definitions
col_statusbar= "#444444" #Status bar colour
col_sidebar = "#282828" #Sidebar background colour
col_canvas = "#181818" #Canvas background colour

col_vis = "#2fbfc6" #vis colour flair
col_nir = "#f23e53" #nir colour flair
col_setget = "#2fc62f" #setget colour flair
col_twit = "#2fbec6" #twitter colour flair
col_sel = "#2fc631" #image setting selection colour flair
col_buttons = "#c62f48" #buttons flair colour

col_inner = "#444444"
col_inner2 = "#eaeaea"

col_but = "#888888" #button array colour

col_set = col_inner #set get button colour flair
col_get = col_inner

col_saveload = "#f7e6be"

class Main:
    def __init__(self, master):
        self.master = master
        #master.title = "AROS Ground Control Software"



        #define variables
        self.status_var = StringVar() #Label for status bar
        self.status_var.set("Things are just dandy")

        self.imgwidth = 640
        self.imgheight = 480

        # default to script directory
        self.imgdir = "."
        self.imgdir_var = StringVar() #Label for file directory location
        self.imgdir_var.set(".")

        self.port_var = StringVar() #Label for selected serial port
        self.port_var.set("No port selected!")

        self.conn_var = StringVar()  # Label for selected serial port
        self.conn_var.set("N/A")

        self.filetype_var = IntVar() #Variable for image filetype selection
        self.filetype_var.set(1)

        self.conntype_var = IntVar()  # Variable for FIFO (1), UART (2) comm type selection
        self.conntype_var.set(1)

        self.canvasimage_unf = Image.open("img/mtn.jpg")
        self.canvasimage = ImageTk.PhotoImage(self.canvasimage_unf)

        #Define button images
        self.read_img = ImageTk.PhotoImage(Image.open("ico/read.png")) # This might not be the most concise way to do this
        self.save_img = ImageTk.PhotoImage(Image.open("ico/save.png"))
        self.reset_img = ImageTk.PhotoImage(Image.open("ico/refresh.png"))

        self.img_scale = 1 #image scale
        self.img_scale_factor = 2 #amount zoomed in/out per click

        # Vis/nir selection checkboxes
        self.vis_sel_var = IntVar()
        self.nir_sel_var = IntVar()




        #Define all major containers
        self.sidebar_frame = Frame(master, bg = col_sidebar, width = 20, height = 20,padx = padding, pady = padding) #Sidebar frame (settings and controls)

        self.title_frame = Frame(self.sidebar_frame, bg = col_sidebar, padx = padding, pady = padding)
        self.vis_frame = Frame(self.sidebar_frame, bg=col_vis, width=20, height=20, padx = padding, pady=padding) #VIS Settings
        self.nir_frame = Frame(self.sidebar_frame, bg=col_nir, width=20, height=20, padx = padding, pady=padding) #NIR Settings
        self.setget_frame = Frame(self.sidebar_frame, bg=col_setget, padx=padding, pady=padding)  # GET SET buttons
        self.serial_frame = Frame(self.sidebar_frame, bg=col_inner, padx=padding, pady=padding)  # Save, load, reset buttons

        self.twitter_frame = Frame(self.sidebar_frame, bg = col_twit, padx = padding, pady = padding) #Twitter send frame

        self.sel_frame = Frame(self.sidebar_frame, padx = padding, pady = padding, bg = col_sel) #spectrum selection box

        self.buttons_frame = Frame(self.sidebar_frame, bg = col_buttons,width=20, height=20, padx=padding, pady=padding) #Control buttons

        self.image_frame = Frame(master, bg = col_canvas,width=200, height=200, padx = 0, pady=0) #Image Viewer
        self.image_toolbar = Frame(self.image_frame, bg = col_sidebar,width = 30, padx = 0, pady = 0) #toolbar

        self.status_bar = Frame(master,bg = col_statusbar,height = 1) #status bar



        #Layout major containers
        self.sidebar_frame.grid(row=0, column=0, sticky = "nsew")
        self.image_frame.grid(  row=0, column=1, sticky = "nsew")
        self.status_bar.grid(row = 1, columnspan = 2, sticky = "nsew")

        self.image_toolbar.pack(side = RIGHT, fill = Y)

        self.title_frame.pack(fill = X, side = TOP)
        self.vis_frame.pack(fill = X, side = TOP)
        self.nir_frame.pack(fill = X, side = TOP)
        self.setget_frame.pack(fill = X, side = TOP)
        self.serial_frame.pack(fill=X, side=TOP)

        self.buttons_frame.pack(fill=X, side=BOTTOM)
        self.sel_frame.pack(fill = X, side = BOTTOM)
        self.twitter_frame.pack(fill=X, side=BOTTOM)


        self.master.grid_columnconfigure(1, weight=1)
        self.master.grid_rowconfigure(0, weight=1)

        # title_frame widgets
        self.title_label = Label(self.title_frame, text = "AROS GCS", bg = col_sidebar, fg = "white", font = ("consolas",22, "bold")).pack()


        # vis_frame widgets
        self.vis_label = Label(self.vis_frame, text = "VIS Sensor Settings",font = ("consolas",14, "bold"),bg = col_sidebar,fg = "white").grid(row = 0, column = 0, columnspan = 5, sticky="nsew")

        self.v_e1_label = Label(self.vis_frame, text = "ISO:",bg = col_inner,fg = "white").grid(row = 1, column = 0, sticky="nsew")
        self.v_e1 = Text(self.vis_frame,height = 1, width = entry_width, bg = "#efefef",highlightbackground = col_inner)
        self.v_e1.grid(row=1, column=1, sticky="nsew")
        self.v_e2_label = Label(self.vis_frame, text="EXP:",bg = col_inner,fg = "white").grid(row=1, column=2, sticky="nsew")
        self.v_e2 = Text(self.vis_frame,height = 1, width = entry_width, bg = "#efefef",highlightbackground = col_inner)
        self.v_e2.grid(row=1, column=3, sticky="nsew")
        self.v_e3_label = Label(self.vis_frame, text="IDK:",bg = col_inner,fg = "white").grid(row=2, column=0, sticky="nsew")
        self.v_e3 = Text(self.vis_frame, height=1, width=entry_width, bg="#efefef",highlightbackground = col_inner)
        self.v_e3.grid(row=2, column=1, sticky="nsew")
        self.v_e4_label = Label(self.vis_frame, text="LOL:",bg = col_inner,fg = "white").grid(row=2, column=2, sticky="nsew")
        self.v_e4 = Text(self.vis_frame, height=1, width=entry_width, bg="#efefef",highlightbackground = col_inner)
        self.v_e4.grid(row=2, column=3, sticky="nsew")

        self.vis_frame.grid_columnconfigure(1, weight=1) #auto-expand
        self.vis_frame.grid_columnconfigure(3, weight=1)


        # nir_frame widgets
        self.nir_label = Label(self.nir_frame, text="NIR Sensor Settings", bg = col_sidebar, fg = "white",font = ("consolas",14, "bold")).grid(row=0, column=0, columnspan=5, sticky="nsew")

        self.n_e1_label = Label(self.nir_frame, text="ISO:",bg = col_inner,fg = "white").grid(row=1, column=0, sticky="nsew")
        self.n_e1 = Text(self.nir_frame, height=1, width=entry_width, bg="#efefef",highlightbackground = col_inner)
        self.n_e1.grid(row=1, column=1, sticky="nsew")
        self.n_e2_label = Label(self.nir_frame, text="EXP:",bg = col_inner,fg = "white").grid(row=1, column=2, sticky="nsew")
        self.n_e2 = Text(self.nir_frame, height=1, width=entry_width, bg="#efefef",highlightbackground = col_inner)
        self.n_e2.grid(row=1, column=3, sticky="nsew")
        self.n_e3_label = Label(self.nir_frame, text="IDK:",bg = col_inner,fg = "white").grid(row=2, column=0, sticky="nsew")
        self.n_e3 = Text(self.nir_frame, height=1, width=entry_width, bg="#efefef",highlightbackground = col_inner)
        self.n_e3.grid(row=2, column=1, sticky="nsew")
        self.n_e4_label = Label(self.nir_frame, text="LOL:",bg = col_inner,fg = "white").grid(row=2, column=2, sticky="nsew")
        self.n_e4 = Text(self.nir_frame, height=1, width=entry_width, bg="#efefef",highlightbackground = col_inner)
        self.n_e4.grid(row=2, column=3, sticky="nsew")

        self.nir_frame.grid_columnconfigure(1, weight=1) #auto-expand
        self.nir_frame.grid_columnconfigure(3, weight=1)


        #SET GET frame widgets
        self.getset_label = Label(self.setget_frame, text="Module Parameters", font = ("consolas",14, "bold"), bg = col_sidebar, fg = "white").grid(row = 0,columnspan = 6, sticky = "nsew")
        self.n_set = Button(self.setget_frame, text="SET", command=self.doNothing, highlightbackground = col_set).grid(row=1, column=0, columnspan = 3, sticky="nsew")
        self.n_get = Button(self.setget_frame, text="GET", command=self.doNothing, highlightbackground = col_get).grid(row=1, column=3, columnspan = 3, sticky="nsew")

        self.s_button = Button(self.setget_frame, text="Save", image = self.save_img, command=self.save_values,
                               highlightbackground=col_sidebar).grid(row=2, column=0, columnspan=2, sticky="nsew")
        self.l_button = Button(self.setget_frame, text="Load", image = self.read_img, command=self.load_values,
                               highlightbackground=col_sidebar).grid(row=2, column=2, columnspan=2, sticky="nsew")
        self.r_button = Button(self.setget_frame, text="Reset",image = self.reset_img, command=self.reset_values,
                               highlightbackground=col_sidebar).grid(row=2, column=4, columnspan=2, sticky="nsew")

        for i in range(6):
            self.setget_frame.grid_columnconfigure(i, weight=1) #make all columns expand equally


        #Serial frame widgets
        self.serial_label = Label(self.serial_frame, text="Serial Parameters", font=("consolas", 14, "bold"), bg=col_sidebar, fg="white").grid(row=0, columnspan=6, sticky="nsew")
        self.serial_list = Listbox(self.serial_frame, selectmode = "SINGLE", height = 1, bg = col_inner, fg = "white", highlightcolor = "#7898cc")
        self.serial_refresh = Button(self.serial_frame, text = "Refresh", command = self.refresh_serial, highlightbackground = col_sidebar).grid(row = 2, columnspan = 3, sticky = "nsew")
        self.serial_assign = Button(self.serial_frame, text = "Assign", command = self.assign_serial, highlightbackground = col_sidebar).grid(row = 2, column = 3, columnspan = 3, sticky = "nsew")
        self.port_label = Label(self.serial_frame, textvariable=self.port_var, font=("consolas", 10), bg=col_sidebar, fg = "white").grid(row=3, columnspan=6, sticky="nsew")
        self.connect_button = Button(self.serial_frame, text = "Verify", command = self.doNothing, highlightbackground = col_sidebar).grid(row = 4, columnspan = 4, sticky = "nsew")
        self.conn_label = Label(self.serial_frame, textvariable=self.conn_var, font=("consolas", 12), bg=col_sidebar,
                                fg="white").grid(row=4, columnspan = 2, column = 4, sticky = "nsew")

        #self.fifo_sel = Radiobutton(self.serial_frame, text="FIFO", variable=self.conntype_var, value=1, bg=col_sidebar, fg = "white")
        #self.fifo_sel.grid(row=4, columnspan=3, sticky="nsew")
        #self.uart_sel = Radiobutton(self.serial_frame, text="UART", variable=self.conntype_var, value=2, bg=col_inner2)
        #self.uart_sel.grid(row=4, columnspan=3, column = 3, sticky="nsew")



        self.serial_list.grid(row = 1, columnspan = 6, sticky = "nsew")
        for i in range(6):
            self.serial_frame.grid_columnconfigure(i, weight=1) #make all columns expand equally


        #Twitter frame widgets
        self.twit_title = Label(self.twitter_frame,text = "Post to Twitter", bg = col_sidebar, fg = "white", font = ("consolas",14, "bold")).pack(fill = BOTH)
        self.twit_message = Text(self.twitter_frame, height=2, width=20, bg="#efefef", highlightbackground = col_inner)
        self.twit_message.pack(fill = BOTH)
        self.twit_send = Button(self.twitter_frame, text = "Post", command = self.tweetImage, highlightbackground = col_sidebar).pack(fill = BOTH)


        # Image file setting frame widgets
        self.sel_label = Label(self.sel_frame, text = "JPEG Settings", bg = col_sidebar, fg = "white", font = ("consolas",14, "bold")).grid(row = 0, column = 0, columnspan = 5, sticky = "nsew")
        #self.vis_sel = Checkbutton(self.sel_frame, text = "Visible",bg = col_inner2).grid(row = 1, column = 0, sticky = "nsew")
        ##self.nir_sel = Checkbutton(self.sel_frame, text="Near-Infrared", bg = col_inner2).grid(row = 1, column = 1, sticky = "nsew")
        #self.jpg_sel = Radiobutton(self.sel_frame, text=".jpg", variable = self.filetype_var, value = 1,bg = col_inner2)
        #self.jpg_sel.grid(row = 2, column = 0, sticky = "nsew")
        #self.png_sel = Radiobutton(self.sel_frame, text=".png", variable = self.filetype_var, value = 2,bg = col_inner2)
        #self.png_sel.grid(row=2, column=1, sticky="nsew")
        self.imgdir_label = Label(self.sel_frame, text="Output Directory:",bg = col_inner2,fg = "black")
        self.imgdir_label.grid(row=1, columnspan = 2, sticky="nsew")
        self.imgdir_text = Label(self.sel_frame, textvariable = self.imgdir_var,font=("consolas",10),bg =col_inner2).grid(row = 2,column=0, columnspan=3, sticky = "nsew")
        self.directory_button = Button(self.sel_frame, text = "BROWSE",command = self.getImageDir,highlightbackground = col_inner2).grid(row = 1, column=2, columnspan = 1, sticky = "nsew")
        self.width_label = Label(self.sel_frame, text="WIDTH:",bg = col_inner2,fg = "black")
        self.width_label.grid(row=3, column=0, sticky="nsew")
        self.width_text = Text(self.sel_frame, height=1, width=entry_width, bg="#efefef",highlightbackground = col_inner2)
        self.width_text.grid(row=3, column=1, columnspan=2, sticky="nsew")
        self.height_label = Label(self.sel_frame, text="HEIGHT:",bg = col_inner2,fg = "black")
        self.height_label.grid(row=4, column=0, sticky="nsew")
        self.height_text = Text(self.sel_frame, height=1, width=entry_width, bg="#efefef",highlightbackground = col_inner2)
        self.height_text.grid(row=4, column=1, columnspan=2, sticky="nsew")
        self.size_set_button = Button(self.sel_frame, text ="SET", command=self.set_size, highlightbackground = col_inner2)
        self.size_set_button.grid(row=3, column=2,rowspan=2,columnspan=1, sticky="nsew")

        for i in range(5):
            self.sel_frame.grid_columnconfigure(i, weight=1) #make all columns expand equally


        # buttons_frame widgets
        self.c_button_uart = Button(self.buttons_frame, text ="Serial Download", command=self.download, highlightbackground = col_inner).pack(fill = BOTH)
        #self.d_button = Button(self.buttons_frame, text = "DL", command = self.download, highlightbackground = col_inner).grid(row = 0, column = 1,sticky = "nsew")
        self.open_button = Button(self.buttons_frame, text="Open...", command=self.load_image, highlightbackground=col_inner).pack(fill = BOTH)


        # image_frame widgets
        self.canvas = Canvas(self.image_frame, width = 1200, height = 900, bg = col_canvas)
        self.canvas.bind("<Button-1>",self.img_reposition)
        self.canvas.pack(side = TOP, anchor = W)
        #self.canvas.create_oval(200,100,600,500, fill = 'blue')
        self.img_object = self.canvas.create_image(self.canvas.winfo_width()/2,self.canvas.winfo_height()/2, image = self.canvasimage)
        self.canvas.move(self.img_object,self.canvas.winfo_width()/2,self.canvas.winfo_height()/2)
        # TODO Fix img_object from appearing at 0,0 for some dumb reason

        self.z_in = Button(self.image_toolbar,text = "[+]", command = self.zoom_in, highlightbackground = col_sidebar).pack()
        self.z_out = Button(self.image_toolbar, text="[-]", command = self.zoom_out,highlightbackground = col_sidebar).pack()
        self.z_reset = Button(self.image_toolbar, text="[ ]", command = self.zoom_reset, highlightbackground = col_sidebar).pack()
        self.z_fit = Button(self.image_toolbar, text = "Fit", command = self.zoom_fit, highlightbackground = col_sidebar).pack()
        self.z_fill = Button(self.image_toolbar, text = "Fill", command = self.zoom_fill, highlightbackground = col_sidebar).pack()


        # status bar
        self.status = Label(self.status_bar, textvariable = self.status_var,bg = col_statusbar, fg = "white").pack(side = RIGHT)

        ###### Menubar ######
        menubar = Menu(master)
        filemenu = Menu(menubar, tearoff=0)
        windowmenu = Menu(menubar, tearoff=0)

        #File menu
        filemenu.add_command(label="Open...", command=self.load_image)
        #filemenu.add_command(label="Fit", command=self.zoom_fit)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=master.quit)

        #Window menu
        windowmenu.add_command(label="Dark Theme", command = self.doNothing)
        windowmenu.add_command(label="Light Theme", command = self.doNothing)

        #Add menus
        menubar.add_cascade(label="File", menu=filemenu)
        menubar.add_cascade(label="Window", menu=windowmenu)
        master.config(menu=menubar)

        # autoconnect to highest serial port
        try:
            self.refresh_serial()
            self.port_var.set(self.serial_list.get(self.serial_list.size()-1))
        except:
            pass

    def doNothing(self):
        print("Doing nothing...")
        self.status_var.set("Oh my god. Something touched me.")


    def tweetImage(self):
        print("Posting to Twitter")
        self.status_var.set("Image sent to Twitter!")
        post_image(self.active_img, self.twit_message.get("1.0", "end-1c") )

    def getImageDir(self):
        self.imgdir = filedialog.askdirectory()
        print(self.imgdir)
        self.status_var.set(("Image directory set to: " + self.imgdir))
        if len(self.imgdir) < 30:
            self.imgdir_var.set(self.imgdir)
        else:
            self.imgdir_var.set(".."+self.imgdir[(len(self.imgdir)-30):])

    ########## Image functions ###########

    def zoom_in(self):
        self.img_scale *= self.img_scale_factor

        ix,iy = self.canvas.coords(self.img_object)
        nx = (ix - self.canvas.winfo_width()  / 2) * self.img_scale_factor + self.canvas.winfo_width()  / 2
        ny =( iy - self.canvas.winfo_height() / 2) * self.img_scale_factor + self.canvas.winfo_height() / 2

        self.img_redraw(nx,ny)
        self.status_var.set("Zoom factor: " + str(self.img_scale))

    def zoom_out(self):
        self.img_scale *= (1 / self.img_scale_factor)

        ix, iy = self.canvas.coords(self.img_object)
        nx = (ix - self.canvas.winfo_width() / 2) / self.img_scale_factor + self.canvas.winfo_width() / 2
        ny = (iy - self.canvas.winfo_height() / 2) / self.img_scale_factor + self.canvas.winfo_height() / 2

        self.img_redraw(nx,ny)
        self.status_var.set("Zoom factor: " + str(self.img_scale))

    def zoom_reset(self):
        self.canvas.coords(self.img_object,self.canvas.winfo_width()/2,self.canvas.winfo_height()/2)
        self.img_scale = 1

        self.img_redraw(self.canvas.winfo_width()/2, self.canvas.winfo_height()/2)
        self.status_var.set("Zoom factor: " + str(self.img_scale))

    def zoom_fit(self):
        imw, imh = self.canvasimage_unf.size #Get width/ height of image/canvas
        cvw = self.canvas.winfo_width()
        cvh = self.canvas.winfo_height()
        if imw/imh > cvw/cvh:
            print("Image aspect wider than canvas")
            self.img_scale = cvw/imw
        else:
            print("Canvas aspect wider than (or equal to) Image")
            self.img_scale = cvh/imh
        self.img_redraw(self.canvas.winfo_width() / 2, self.canvas.winfo_height() / 2)
        self.status_var.set("Zoom factor: " + str(self.img_scale))

    def zoom_fill(self):
        imw, imh = self.canvasimage_unf.size #Get width/ height of image/canvas
        cvw = self.canvas.winfo_width()
        cvh = self.canvas.winfo_height()
        if imw/imh > cvw/cvh:
            print("Image aspect wider than canvas")
            self.img_scale = cvh/imh
        else:
            print("Canvas aspect wider than (or equal to) Image")
            self.img_scale = cvw/imw
        self.img_redraw(self.canvas.winfo_width() / 2, self.canvas.winfo_height() / 2)
        self.status_var.set("Zoom factor: " + str(self.img_scale))


    def img_reposition(self, event):
        print("clicked at:", event.x, event.y)
        self.imgx, self.imgy = self.canvas.coords(self.img_object)
        self.canvas.move(self.img_object, -(event.x - self.canvas.winfo_width()/2), -(event.y - self.canvas.winfo_height()/2))

    def img_redraw(self,x,y):
        # self.canvas.scale(self.img_object,0,0, self.img_scale, self.img_scale) # ONLY WORKS FOR VECTOR OBJECTS :(
        if self.img_object:
            self.canvas.delete(self.img_object)
        iw, ih = self.canvasimage_unf.size

        size = int(iw * self.img_scale), int(ih * self.img_scale)
        self.canvasimage = ImageTk.PhotoImage(self.canvasimage_unf.resize(size))

        self.img_object = self.canvas.create_image(x,y, image=self.canvasimage)

    def load_image(self):
        try: self.active_img = filedialog.askopenfilename(initialdir = self.imgdir, filetypes = (("Image files","*.jpg"),("Image files","*.jpeg"),("Image files","*.png")))
        except: self.active_img = filedialog.askopenfilename(filetypes = (("Image files","*.jpg"),("Image files","*.jpeg"),("Image files","*.png")))
        self.canvasimage_unf = Image.open(self.active_img)
        self.zoom_reset()
        self.status_var.set("New image loaded")

    ########### Config File functions ###########

    def load_values(self):
        vals = config_handler.load_user_values() #overwrite vals (is only used as a workaround where needed)
        self.v_e1.delete(1.0, END)
        self.v_e2.delete(1.0, END)
        self.v_e3.delete(1.0, END)
        self.v_e4.delete(1.0, END)
        self.n_e1.delete(1.0, END)
        self.n_e2.delete(1.0, END)
        self.n_e3.delete(1.0, END)
        self.n_e4.delete(1.0, END)
        self.v_e1.insert(END, vals['v_e1'])
        self.v_e2.insert(END, vals['v_e2'])
        self.v_e3.insert(END, vals['v_e3'])
        self.v_e4.insert(END, vals['v_e4'])
        self.n_e1.insert(END, vals['n_e1'])
        self.n_e2.insert(END, vals['n_e2'])
        self.n_e3.insert(END, vals['n_e3'])
        self.n_e4.insert(END, vals['n_e4'])
        self.width_text.delete(1.0, END)
        self.height_text.delete(1.0, END)
        self.width_text.insert(END, "{0}".format(self.imgwidth))
        self.height_text.insert(END, "{0}".format(self.imgheight))
        self.status_var.set("Loaded values")

    def save_values(self):
        pv['v_e1'] = self.v_e1.get("1.0", "end-1c")
        pv['v_e2'] = self.v_e2.get("1.0", "end-1c")
        pv['v_e3'] = self.v_e3.get("1.0", "end-1c")
        pv['v_e4'] = self.v_e4.get("1.0", "end-1c")
        pv['n_e1'] = self.n_e1.get("1.0", "end-1c")
        pv['n_e2'] = self.n_e2.get("1.0", "end-1c")
        pv['n_e3'] = self.n_e3.get("1.0", "end-1c")
        pv['n_e4'] = self.n_e4.get("1.0", "end-1c")
        config_handler.save_current_values(pv)
        self.status_var.set("Saved values")

    def reset_values(self):
        vals = config_handler.reset_default_values()
        self.load_values()
        self.status_var.set("Resetted values")

    ########## Image Settings ###########

    def set_size(self):
        self.imgwidth = int(self.width_text.get("1.0", "end-1c"))
        self.imgheight = int(self.height_text.get("1.0", "end-1c"))

    ########## Image Capture functions ###########

    def download(self): # Download image from serial.
        print("Download process started.")
        self.active_img = serial_handler.snap(self.port_var.get(), self.imgwidth, self.imgheight, self.imgdir)
        self.canvasimage_unf = Image.open(self.active_img)
        self.zoom_reset()
        self.status_var.set("New image loaded")

    def refresh_serial(self):
        hg = 0
        self.status_var.set("Refreshing Serial List...")
        serial_ports = serial_handler.serial_ports()
        print(serial_ports)
        self.serial_list.delete(0, END)
        for item in serial_ports:
            self.serial_list.insert(END,item)
            hg += 1
        if hg == 0:
            hg = 1
        self.serial_list.config(height = hg)
        self.status_var.set("Serial Port List Refreshed")

    def assign_serial(self):
        print(self.serial_list.get(ACTIVE))
        self.port_var.set(self.serial_list.get(ACTIVE))
        self.status_var.set("Serial Port Assigned")

class Splash:
    def __init__(self, master):
        self.master = master

        #master.title = "AROS Splash Title"

        master.configure(bg = "black")

        self.splash_text = Label(master, text = "Click anywhere to continue. " + versionInfo)
        self.splash_text.configure(bg = "black", fg = "gray")

        self.splash_img = ImageTk.PhotoImage(file="img/splash3.png")
        self.splash_label = Label(master, image=self.splash_img)
        self.splash_label.bind("<Button-1>", self.close)
        self.splash_label.configure(bg = "black")


        self.splash_label.pack()
        self.splash_text.pack(side = RIGHT)

    def close(self, event):
        main_gui.zoom_fit()  # Reset zoom and center image (fixes weird bug where image doesn't want to reposition)
        top1.destroy() # Having difficulty getting master to work here, so used workaround









root = Tk()
root.title("AROS Ground Control Software")

top1 = Toplevel()
top1.title("AGCS")


splash_gui = Splash(top1)
main_gui = Main(root)


top1.lift() #Makes splash the top window

pv = config_handler.load_user_values() #Loads values (pv = parameter values)
main_gui.load_values() #Load saved values


#Place main screen in center of monitor
windowWidth = root.winfo_reqwidth()
windowHeight = root.winfo_reqheight()
positionRight = int(root.winfo_screenwidth()/2 - windowWidth/2)
positionDown = int(root.winfo_screenheight()/2 - windowHeight/2-50)
root.geometry("+{}+{}".format(positionRight, positionDown))

#Place splash screen in center of monitor
windowWidth = top1.winfo_reqwidth()
windowHeight = top1.winfo_reqheight()
positionRight = int(top1.winfo_screenwidth()/2 - windowWidth/2)
positionDown = int(top1.winfo_screenheight()/2 - windowHeight/2)
top1.geometry("+{}+{}".format(positionRight, positionDown))



root.mainloop()




# from tkinter import *
# import tkinter.messagebox
#
# versionInfo = "v4.20"
#
# splash = Tk()  #Splash screen
# splash.title("AROS GSS - Splash")
# splash.resizable(FALSE,FALSE)
# root = Tk()    #Main screen
# root.title("AROS GSS")
#
# # Functions
# def doNothing():
#     print("Yoshi is a war criminal")
#
#
# #Splash ----------------------
#
# #splash_frame = Frame(splash,width = 500, height = 400)
# splash_image = PhotoImage(file = "splash.gif")
# splash_label = Label(splash,image = splash_image)
# #splash_frame.pack()
# splash_label.pack()
#
#
# splash_text1 = Label(splash,text = "AROS Ground Station Software")
# splash_text2 = Label(splash,text = versionInfo)
# splash_text1.pack(side = LEFT)
# splash_text2.pack(side = RIGHT)
#
# splash.after(5000, splash.destroy)
#
#
# #Root ----------------------
#
# #Menu definition:
# menu = Menu(root)
# root.config(menu=menu)
#
# menu_1 = Menu(menu)
# menu.add_cascade(label = "File",menu=menu_1)
# #Menu contents ------
# menu_1.add_command(label = "New", command = doNothing)
# menu_1.add_command(label = "Old", command = doNothing)
# menu_1.add_separator()
# menu_1.add_command(label = "Quit", command = root.quit)
#
# #Root Contents
#
# imageFrame = Frame(root,relief = SUNKEN, bd = 1)
# imageFrame.pack(side = LEFT)
# controlFrame = Frame(root, relief = SUNKEN, bg = "grey")
# controlFrame.pack(side = TOP, fill = Y)
#
# canvas = Canvas(imageFrame, width = 600, height = 600)
# circle = canvas.create_oval(0,0,600,600,fill = "grey")
# canvas_photo = PhotoImage(file = "splash.gif")
# canvas_image = canvas.create_image(canvas, image = canvas_photo)
# canvas.pack()
#
#
# captureB = Button(controlFrame, text = "Capture", command = doNothing)
# twitterB = Button(controlFrame, text = "Upload to Twitter", command = doNothing)
# twitterEntry = Entry(controlFrame)
#
# captureB.pack(side = TOP)
# twitterB.pack(side = TOP)
# twitterEntry.pack(side = TOP)
#
# #Status Bar Contents
#
# status = Label(imageFrame,text = "Status Bar", bd = 1, relief = SUNKEN, anchor = W)
# status.pack(side = BOTTOM, fill = X)
#
#
#
#
#
#
#
# #Place splash screen in center of monitor
# windowWidth = splash.winfo_reqwidth()
# windowHeight = splash.winfo_reqheight()
# positionRight = int(splash.winfo_screenwidth()/2 - 250)
# positionDown = int(splash.winfo_screenheight()/2 - 140)
# splash.geometry("+{}+{}".format(positionRight, positionDown))
#
#
#
# root.mainloop()