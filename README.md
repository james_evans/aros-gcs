# AROS Ground Control Software

This software acts as a GUI interface between a computer and the AROS high-altitude imaging module, allowing the user to send image capture requests, download stored images, and configure module parameters. Currently, this software is developed with easy demonstration in mind, and is not well suited for actual field work where images need to be downlinked from a satellite in orbit.

![mainwindow](doc/main.png)

Main window

![splashwindow](doc/splash.png)

Splash screen

## Required packages:
- `(sudo) pip install tkinter` (may already be present)
- `(sudo) pip install pillow`
- `(sudo) pip3 install twython`
- `(sudo) pip install numpy`
- `(sudo) pip install pyserial`

(Packages for components still in development)
- `(sudo) pip install pylibftdi` Library for FT245R comms
- `(sudo) brew install libftdi (https://brew.sh/)` (Old library for FT245R comms. *Install should not be required*)

## Developer Notes

### A note on UART Image Capture (or how it should work)

The image capture routine is a follows:

*On startup:*
1) Click 'Refresh' under 'Serial Parameters' with the AROS I / II connected. The GUI should detect available serial comm
ports. Select the port for the module and click 'Assign'.
2) Click 'Data Directory' under 'Image File Settings' and choose a location for the data directory where the .csv and 
.jpg/png files will be stored.

*When taking images*

1) Click 'Capt. - UART' to begin the image capture request. This will attempt to download an image using the UART serial
line using RA's serial code, then update the active image with the downloaded image.

Clicking 'Capt. - UART' calls `self.download(1)`, which then calls `serial_handler.csv_download()`. `csv_download()` downloads 
and processes the .csv, and returns the
new image filepath to `self.download()`, which assigns it as the new active image. (As of July 22,
`serial_handler.csv_download()` is not updated to the most recent code by RA.)

### Non-functioning GUI Features

The VIS and NIR Sensor Settings do not modify any registers in the camera. This probably wouldn't be too hard to implement,
but is low priority. Save, load, and reset functionality works however.

'Verify' button exists ideally as a way to check if the module is actually connected over serial after the port is selected,
but is not implemented.

Twitter functionality works currently, but I guess I *should* to modify the code slightly to send the active image instead of squidward
dabbing.

Under 'Image File Settings', 'Visible' and 'Near-Infrared' don't do anything currently BUT '.jpg' and '.png' SHOULD work.
.png seems to produce nicer images (less artifacting).

'Process' button was kind of hijacked for .csv image processing testing. Doesn't have a real-world purpose right now other
than processing whatever .csv is under `[data directory]/csv/testcsv.csv` onto the active window.


Please note that this GUI is still largely untested, so bugs are likely present. `csv_handler.py` is mostly obsolete, replaced with `serial_handler.py` 
for all-in-one .csv download and processing.

## Main Features
- Image capture requests
- Image settings configuration
- Viewing downloaded images
- Uploading images to the [@ArosImaging](https://twitter.com/ArosImaging) Twitter

## Brief Code Overview
This code is built with `tkinter` on `Python 3`. Major widget containers are first defined and placed, laying out the overall shape of the GUI. Each frame's widgets are then defined, with parameters passed to make the window as nicely scalable as possible. Currently, some buttons only call the `doNothing()` function but adding functionality is as easy as adding `command = (a function)` to the button/etc definition.